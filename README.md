Instalacion:
1) Clonar repositiorio y acceder
1) Instalar composer
2) ejecutar en consola "composer install"
4) Migrar base de datos "agencia_viajes.sql"
5) Ejecutar comando en console para crear entidades "php bin/console doctrine:mapping:import "App\Entity" annotation --path=src/Entity"
6) Ejecutar comando en console para crear getters y setters "php bin/console make:entity --regenerate App"
7) Iniciar servidor symfony "php bin/console server:run"
