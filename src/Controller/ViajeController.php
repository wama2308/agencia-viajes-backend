<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\ViajeRepository;
use App\Repository\ViajeroViajesRepository;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\Viajes;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ViajeController extends Controller
{
    /**
     * Descricion: Devuelve un array con los registros activos de la tabla viajes (borrado = 0)   
     * @Route("/api/indexViaje", methods="GET")
     */
    public function indexViajeAction(ViajeRepository $viajeRepository)
    {
        $viajes = $viajeRepository->viajesAll();
        return new JsonResponse($viajes);
    }

    /**
     * Descricion: Devuelve un array con los registros activos de la tabla viajes (borrado = 0) para llenar select referentes a los viajes   
     * @Route("/api/selectViajes", methods="GET")
     */
    public function selectViajesAction(ViajeRepository $viajeRepository)
    {
        $viajes = $viajeRepository->selectViajesAll();
        return new JsonResponse($viajes);
    }

    /**
     * Descricion: Crea un nuevo registro en la tabla viajes             
     * @param codigoViaje  
     * @param nroPlazas  
     * @param origen  
     * @param destino  
     * @param precio  
     * @Route("/api/createViaje", methods="POST")
     */
    public function createViajeAction(Request $request, EntityManagerInterface $em)
    {
        $requestData = json_decode($request->getContent(), true);
        $codigoViaje = $requestData['codigo_viaje'];
        $numeroPlazas = $requestData['numero_plazas'];
        $origen = $requestData['origen'];
        $destino = $requestData['destino'];
        $precio = $requestData['precio'];

        if ($codigoViaje === '') {
            return new Response("Campo codigo de viaje requerido", 422);
        } else if ($numeroPlazas === '') {
            return new Response("Campo numero de plazas requerido", 422);
        } else if ($origen === '') {
            return new Response("Campo origen requerido", 422);
        } else if ($destino === '') {
            return new Response("Campo destino requerido", 422);
        } else {
            $viaje = new Viajes();
            $viaje->setCodigoViaje($codigoViaje);
            $viaje->setNumeroPlazas($numeroPlazas);
            $viaje->setOrigen($origen);
            $viaje->setDestino($destino);
            $viaje->setPrecio($precio);
            $viaje->setBorrado(0);
            $viaje->setCreatedAt(new \DateTime());
            $viaje->setUpdatedAt(new \DateTime());

            $em->persist($viaje);

            $em->flush();

            $nuevoViaje = array(
                "mensaje" => "Viaje guardado con exito",
                "data" => array(
                    "id" => (string)$viaje->getId(),
                    "codigo_viaje" => $viaje->getCodigoViaje(),
                    "numero_plazas" => (string)$viaje->getNumeroPlazas(),
                    "origen" => $viaje->getOrigen(),
                    "destino" => $viaje->getDestino(),
                    "precio" => $viaje->getPrecio()
                )
            );
            return new JsonResponse($nuevoViaje);
        }
    }

    /**
     * Descricion: Devuelve un objeto que contiene la data de un registro de la tabla viajes
     * @param idViaje
     * @Route("/api/indexViajeId/{id}", methods="GET")
     */
    public function indexViajeIdAction($id, ViajeRepository $viajeRepository)
    {
        $viaje = $viajeRepository->viajeId($id);
        return new JsonResponse($viaje);
        //return new Response($id);
    }

    /**
     * Descricion: Actualiza los datos de un registro en la tabla viajes                  
     * @param idViaje
     * @param codigoViaje  
     * @param nroPlazas  
     * @param origen  
     * @param destino  
     * @param precio  
     * @Route("/api/updateViaje/{id}", methods="PUT")
     */
    public function updateViajeAction(Request $request, EntityManagerInterface $em)
    {
        $requestData = json_decode($request->getContent(), true);
        $id = $requestData['id'];
        $codigoViaje = $requestData['codigo_viaje'];
        $numeroPlazas = $requestData['numero_plazas'];
        $origen = $requestData['origen'];
        $destino = $requestData['destino'];
        $precio = $requestData['precio'];

        if ($id === '') {
            return new Response("Campo id requerido", 422);
        } else if ($codigoViaje === '') {
            return new Response("Campo codigo de viaje requerido", 422);
        } else if ($numeroPlazas === '') {
            return new Response("Campo numero de plazas requerido", 422);
        } else if ($origen === '') {
            return new Response("Campo origen requerido", 422);
        } else if ($destino === '') {
            return new Response("Campo destino requerido", 422);
        } else {
            $viaje = $em->getRepository(Viajes::class)->find($id);
            $viaje->setCodigoViaje($codigoViaje);
            $viaje->setNumeroPlazas($numeroPlazas);
            $viaje->setOrigen($origen);
            $viaje->setDestino($destino);
            $viaje->setPrecio($precio);
            $viaje->setBorrado(0);
            $viaje->setCreatedAt(new \DateTime());
            $viaje->setUpdatedAt(new \DateTime());

            $em->flush();

            $nuevoViaje = array(
                "mensaje" => "Viaje actualizado con exito",
                "data" => array(
                    "id" => (string)$viaje->getId(),
                    "codigo_viaje" => $viaje->getCodigoViaje(),
                    "numero_plazas" => (string)$viaje->getNumeroPlazas(),
                    "origen" => $viaje->getOrigen(),
                    "destino" => $viaje->getDestino(),
                    "precio" => $viaje->getPrecio()
                )
            );
            return new JsonResponse($nuevoViaje);
        }
    }

    /**
     * Descricion: Actualiza el valor del campo borrado de un registro de la tabla viajes (eliminacion logica)     
     * @param idViaje
     * @Route("/api/deleteViaje/{id}", methods="PUT")
     */
    public function deleteViajeAction(Request $request, EntityManagerInterface $em, ViajeroViajesRepository $ViajeroViajesRepository)
    {
        $requestData = json_decode($request->getContent(), true);
        $id = $requestData['id'];
        $viaje = $em->getRepository(Viajes::class)->find($id);
        $viaje->setBorrado(1);
        $viaje->setUpdatedAt(new \DateTime());
        $em->flush();

        $ViajeroViajesRepository->borradoViajerosViajeAll($id, 'viaje');

        $viajeEliminado = array(
            "mensaje" => "Viaje eliminado con exito",
            "data" => array(
                "id" => (string)$viaje->getId()
            )
        );
        return new JsonResponse($viajeEliminado);
    }
}
