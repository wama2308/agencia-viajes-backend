<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
//use App\Repository\ViajeRepository;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\ViajeroViajes;
use App\Repository\ViajeroViajesRepository;
use App\Entity\Viajes;
use App\Entity\Viajeros;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ViajeroViajesController extends Controller
{
    /**
     * @Route("/api/createViajeroViajes", methods="POST")
     * @param arrayViajeroViajes
     */
    public function createViajeAction(Request $request, EntityManagerInterface $em)
    {
        $arrayViajeroViaje = [];
        $requestData = json_decode($request->getContent(), true);
        $requestViajeroViaje = $requestData['array_viajero_viaje'];

        foreach ($requestViajeroViaje as $dataViajeroViaje) {
            $viajeroViajes = new ViajeroViajes();
            $viajero = $em->getRepository(Viajeros::class)->find($dataViajeroViaje['viajero_id']);
            $viaje = $em->getRepository(Viajes::class)->find($dataViajeroViaje['viaje_id']);
            $viajeroViajes->setViajero($viajero);
            $viajeroViajes->setViaje($viaje);
            $viajeroViajes->setCreatedAt(new \DateTime());
            $viajeroViajes->setUpdatedAt(new \DateTime());

            $em->persist($viajeroViajes);
            $em->flush();
            array_push($arrayViajeroViaje, array(
                "id" => $viajeroViajes->getId(),
                "viajero_id" => $viajero->getId(),
                "viaje_id" => $viaje->getId()
            ));
        }

        $asignacionViaje = array(
            "mensaje" => "Asignacion de viaje(s) realizada con exito",
            "data" => $arrayViajeroViaje
        );

        return new JsonResponse($asignacionViaje);
    }

    /**
     * Descricion: Devuelve un array con los viajes asociados del viajero
     * @param idViajero       
     * @Route("/api/viajeroViajesId/{id}", methods="GET")
     */
    public function viajeroViajesIdAction($id, ViajeroViajesRepository $ViajeroViajesRepository)
    {
        $viajeroViajes = $ViajeroViajesRepository->selectViajerosViajeId($id);
        return new JsonResponse($viajeroViajes);
        //return new Response($id);
    }
}
