<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\ViajeroRepository;
use App\Repository\ViajeroViajesRepository;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\Viajeros;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ViajeroController extends Controller
{

    /**
     * Descricion: Devuelve un array con los registros activos de la tabla viajeros (borrado = 0)     
     * @Route("/api/indexViajero", methods="GET")     
     */
    public function indexViajeroAction(ViajeroRepository $viajeroRepository)
    {
        $viajeros = $viajeroRepository->viajerosAll();
        return new JsonResponse($viajeros);
    }

    /**
     * Descricion: Crea un nuevo registro en la tabla viajeros                  
     * @param cedula     
     * @param nombre     
     * @param fechaNacimiento     
     * @param telefono     
     * @Route("/api/createViajero", methods="POST")
     */
    public function createViajeroAction(Request $request, EntityManagerInterface $em)
    {
        $requestData = json_decode($request->getContent(), true);
        $nombre = $requestData['nombre'];
        $cedula = $requestData['cedula'];
        $fechaNacimiento = $requestData['fecha_nacimiento'];
        $telefono = $requestData['telefono'];

        if ($nombre === '') {
            return new Response("Campo nombre requerido", 422);
        } else if ($cedula === '') {
            return new Response("Campo cedula requerido", 422);
        } else if ($fechaNacimiento === '') {
            return new Response("Campo fecha de nacimiento requerido", 422);
        } else if ($telefono === '') {
            return new Response("Campo telefono requerido", 422);
        } else {
            $viajero = new Viajeros();
            $viajero->setCedula($cedula);
            $viajero->setNombre($nombre);
            $viajero->setFechaNacimiento(new \DateTime($fechaNacimiento));
            $viajero->setTelefono($telefono);
            $viajero->setBorrado(0);
            $viajero->setCreatedAt(new \DateTime());
            $viajero->setUpdatedAt(new \DateTime());

            $em->persist($viajero);

            $em->flush();
            
            $fechaNacimiento = (string)$viajero->getFechaNacimiento()->format('Y-m-d H:i:s');
            $fechaNacimientoFormat = substr($fechaNacimiento, 0, 10);

            $nuevoViajero = array(
                "mensaje" => "Viaejero guardado con exito",
                "data" => array(
                    "id" => (string)$viajero->getId(),
                    "cedula" => $viajero->getCedula(),
                    "nombre" => $viajero->getNombre(),
                    "fecha_nacimiento" => $fechaNacimientoFormat,
                    "telefono" => $viajero->getTelefono()
                )
            );
            return new JsonResponse($nuevoViajero);
        }
    }

    /**
     * Descricion: Devuelve un objeto que contiene la data de un registro de la tabla viajeros     
     * @param idViajero
     * @Route("/api/indexViajeroId/{id}", methods="GET")
     */
    public function indexViajeroIdAction($id, ViajeroRepository $viajeroRepository)
    {
        $viajero = $viajeroRepository->viajeroId($id);
        return new JsonResponse($viajero);
        //return new Response($id);
    }

    /**
     * Descricion: Actualiza los datos de un registro en la tabla viajeros     
     * @param idViajero     
     * @param cedula     
     * @param nombre     
     * @param fechaNacimiento     
     * @param telefono     
     * @Route("/api/updateViajero/{id}", methods="PUT")
     */
    public function updateViajeroAction(Request $request, EntityManagerInterface $em)
    {
        $requestData = json_decode($request->getContent(), true);
        $id = $requestData['id'];
        $nombre = $requestData['nombre'];
        $cedula = $requestData['cedula'];
        $fechaNacimiento = $requestData['fecha_nacimiento'];
        $telefono = $requestData['telefono'];

        if ($id === '') {
            return new Response("Campo id requerido", 422);
        } else if ($nombre === '') {
            return new Response("Campo nombre requerido", 422);
        } else if ($cedula === '') {
            return new Response("Campo cedula requerido", 422);
        } else if ($fechaNacimiento === '') {
            return new Response("Campo fecha de nacimiento requerido", 422);
        } else if ($telefono === '') {
            return new Response("Campo telefono requerido", 422);
        } else {
            $viajero = $em->getRepository(Viajeros::class)->find($id);
            $viajero->setCedula($cedula);
            $viajero->setNombre($nombre);
            $viajero->setFechaNacimiento(new \DateTime($fechaNacimiento));
            $viajero->setTelefono($telefono);
            $viajero->setBorrado(0);
            $viajero->setUpdatedAt(new \DateTime());

            $em->flush();

            $fechaNacimiento = (string)$viajero->getFechaNacimiento()->format('Y-m-d H:i:s');
            $fechaNacimientoFormat = substr($fechaNacimiento, 0, 10);

            $viajeroActualizado = array(
                "mensaje" => "Viajero actualizado con exito",
                "data" => array(
                    "id" => (string)$viajero->getId(),
                    "cedula" => $viajero->getCedula(),
                    "nombre" => $viajero->getNombre(),
                    "fecha_nacimiento" => $fechaNacimientoFormat,
                    "telefono" => $viajero->getTelefono()
                )
            );
            return new JsonResponse($viajeroActualizado);
        }
    }

    /**
     * Descricion: Actualiza el valor del campo borrado de un registro de la tabla viajeros (eliminacion logica)     
     * @param idViajero
     * @Route("/api/deleteViajero/{id}", methods="PUT")
     */
    public function deleteViajeroAction(Request $request, EntityManagerInterface $em, ViajeroViajesRepository $ViajeroViajesRepository)
    {
        $requestData = json_decode($request->getContent(), true);
        $id = $requestData['id'];

        $viajero = $em->getRepository(Viajeros::class)->find($id);
        $viajero->setBorrado(1);
        $viajero->setUpdatedAt(new \DateTime());

        $em->flush();

        $ViajeroViajesRepository->borradoViajerosViajeAll($id, 'viajero');

        $viajeroEliminado = array(
            "mensaje" => "Viajero eliminado con exito",
            "data" => array(
                "id" => (string)$viajero->getId()
            )
        );
        return new JsonResponse($viajeroEliminado);
    }
}
