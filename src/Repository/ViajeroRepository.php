<?php

namespace App\Repository;

use App\Entity\Viajeros;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;


class ViajeroRepository extends ServiceEntityRepository
{
    public function __construct(\Doctrine\Persistence\ManagerRegistry $registry)
    {
        parent::__construct($registry, Viajeros::class);
    }

    public function viajerosAll()
    {
        $viajerosArray = [];
        $em = $this->getEntityManager()->getConnection();
        $query = "SELECT * FROM viajeros where borrado = 0;";        
        $stmt = $em->prepare($query);
        $params = array();
        $stmt->execute($params);
        $dataViajeros = $stmt->fetchAll();
        foreach ($dataViajeros as $viajero) {
            $nuevoViajero = array(
                "id" => $viajero['id'],
                "cedula" => $viajero['cedula'],
                "nombre" => $viajero['nombre'],
                "fecha_nacimiento" => $viajero['fecha_nacimiento'],
                "telefono" => $viajero['telefono']
            );
            array_push($viajerosArray, $nuevoViajero);
        }

        return $viajerosArray;
    }

    public function viajeroId($id)
    {
        $em = $this->getEntityManager()->getConnection();
        $query = "SELECT * FROM viajeros where id = " . $id;
        $stmt = $em->prepare($query);
        $params = array();
        $stmt->execute($params);
        $dataViajero = $stmt->fetchAll();
        
        $viajero = array(
            "id" => $dataViajero[0]['id'],
            "cedula" => $dataViajero[0]['cedula'],
            "nombre" => $dataViajero[0]['nombre'],
            "fecha_nacimiento" => $dataViajero[0]['fecha_nacimiento'],
            "telefono" => $dataViajero[0]['telefono']
        );

        return $viajero;
    }
}
