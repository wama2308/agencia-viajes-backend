<?php

namespace App\Repository;

use App\Entity\ViajeroViajes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;


class ViajeroViajesRepository extends ServiceEntityRepository
{
    public function __construct(\Doctrine\Persistence\ManagerRegistry $registry)
    {
        parent::__construct($registry, ViajeroViajes::class);
    }

    public function borradoViajerosViajeAll($id, $option)
    {
        if ($option === 'viajero') {
            $em = $this->getEntityManager()->getConnection();
            $statement = $em->prepare("UPDATE viajero_viajes SET borrado=1 WHERE viajero_id=" . $id);
            $statement->execute();
        } else {
            $em = $this->getEntityManager()->getConnection();
            $statement = $em->prepare("UPDATE viajero_viajes SET borrado=1 WHERE viaje_id=" . $id);
            $statement->execute();
        }
        return 1;
    }

    public function selectViajerosViajeId($id)
    {
        $viajerosViajeIdArray = [];
        $em = $this->getEntityManager()->getConnection();
        $query = "SELECT 
        viajero_viajes.id,
        viajero_viajes.viaje_id,
        viajero_viajes.viajero_id,
        viajes.codigo_viaje,
        viajes.origen,
        viajes.destino,
        viajes.precio
        FROM viajero_viajes, viajes, viajeros 
        WHERE viajero_viajes.viajero_id = viajeros.id
        AND viajero_viajes.viaje_id = viajes.id
        AND viajero_viajes.borrado = 0
        AND viajeros.id = " . $id;
        $stmt = $em->prepare($query);
        $params = array();
        $stmt->execute($params);
        $dataViajerosViaje = $stmt->fetchAll();
        foreach ($dataViajerosViaje as $viajeroViaje) {
            $travelViajeroViajes = array(
                "id" => $viajeroViaje['id'],
                "viaje_id" => $viajeroViaje['viaje_id'],
                "viajero_id" => $viajeroViaje['viajero_id'],
                "codigo_viaje" => $viajeroViaje['codigo_viaje'],
                "origen" => $viajeroViaje['origen'],
                "destino" => $viajeroViaje['destino'],
                "precio" => $viajeroViaje['precio']
            );
            array_push($viajerosViajeIdArray, $travelViajeroViajes);
        }

        return $viajerosViajeIdArray;
    }
}
