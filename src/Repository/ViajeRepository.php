<?php

namespace App\Repository;

use App\Entity\Viajes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;


class ViajeRepository extends ServiceEntityRepository
{
    public function __construct(\Doctrine\Persistence\ManagerRegistry $registry)
    {
        parent::__construct($registry, Viajes::class);
    }

    public function viajesAll()
    {
        $viajesArray = [];
        $em = $this->getEntityManager()->getConnection();
        $query = "SELECT * FROM viajes where borrado = 0;";
        $stmt = $em->prepare($query);
        $params = array();
        $stmt->execute($params);
        $dataViajes = $stmt->fetchAll();
        foreach ($dataViajes as $viaje) {
            $nuevoViaje = array(
                "id" => $viaje['id'],
                "codigo_viaje" => $viaje['codigo_viaje'],
                "numero_plazas" => $viaje['numero_plazas'],
                "origen" => $viaje['origen'],
                "destino" => $viaje['destino'],
                "precio" => $viaje['precio']
            );
            array_push($viajesArray, $nuevoViaje);
        }

        return $viajesArray;
    }

    public function selectViajesAll()
    {
        $viajesArray = [];
        $em = $this->getEntityManager()->getConnection();
        $query = "SELECT * FROM viajes where borrado = 0;";
        $stmt = $em->prepare($query);
        $params = array();
        $stmt->execute($params);
        $dataViajes = $stmt->fetchAll();
        foreach ($dataViajes as $viaje) {
            $label = $viaje['codigo_viaje'] . ' - ' . $viaje['origen'] . ' - ' . $viaje['destino'];
            $nuevoViaje = array(
                "value" => $viaje['id'],
                "label" => $label
            );
            array_push($viajesArray, $nuevoViaje);
        }

        return $viajesArray;
    }

    public function viajeId($id)
    {
        $em = $this->getEntityManager()->getConnection();
        $query = "SELECT * FROM viajes where id = " . $id;
        $stmt = $em->prepare($query);
        $params = array();
        $stmt->execute($params);
        $dataViajero = $stmt->fetchAll();

        $viaje = array(
            "id" => $dataViajero[0]['id'],
            "codigo_viaje" => $dataViajero[0]['codigo_viaje'],
            "numero_plazas" => $dataViajero[0]['numero_plazas'],
            "origen" => $dataViajero[0]['origen'],
            "destino" => $dataViajero[0]['destino'],
            "precio" => $dataViajero[0]['precio']
        );

        return $viaje;
    }
}
