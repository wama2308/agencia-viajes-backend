<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ViajeroViajes
 *
 * @ORM\Table(name="viajero_viajes", indexes={@ORM\Index(name="viajero_fk", columns={"viajero_id"}), @ORM\Index(name="viaje_fk", columns={"viaje_id"})})
 * @ORM\Entity
 */
class ViajeroViajes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="borrado", type="integer", nullable=false)
     */
    private $borrado = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var \Viajes
     *
     * @ORM\ManyToOne(targetEntity="Viajes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="viaje_id", referencedColumnName="id")
     * })
     */
    private $viaje;

    /**
     * @var \Viajeros
     *
     * @ORM\ManyToOne(targetEntity="Viajeros")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="viajero_id", referencedColumnName="id")
     * })
     */
    private $viajero;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBorrado(): ?int
    {
        return $this->borrado;
    }

    public function setBorrado(int $borrado): self
    {
        $this->borrado = $borrado;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getViaje(): ?Viajes
    {
        return $this->viaje;
    }

    public function setViaje(?Viajes $viaje): self
    {
        $this->viaje = $viaje;

        return $this;
    }

    public function getViajero(): ?Viajeros
    {
        return $this->viajero;
    }

    public function setViajero(?Viajeros $viajero): self
    {
        $this->viajero = $viajero;

        return $this;
    }


}
