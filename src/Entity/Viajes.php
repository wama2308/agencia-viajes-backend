<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Viajes
 *
 * @ORM\Table(name="viajes")
 * @ORM\Entity
 */
class Viajes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_viaje", type="string", length=50, nullable=false)
     */
    private $codigoViaje;

    /**
     * @var int
     *
     * @ORM\Column(name="numero_plazas", type="integer", nullable=false)
     */
    private $numeroPlazas;

    /**
     * @var string
     *
     * @ORM\Column(name="origen", type="string", length=50, nullable=false)
     */
    private $origen;

    /**
     * @var string
     *
     * @ORM\Column(name="destino", type="string", length=50, nullable=false)
     */
    private $destino;

    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float", precision=10, scale=2, nullable=false)
     */
    private $precio;

    /**
     * @var int
     *
     * @ORM\Column(name="borrado", type="integer", nullable=false)
     */
    private $borrado = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodigoViaje(): ?string
    {
        return $this->codigoViaje;
    }

    public function setCodigoViaje(string $codigoViaje): self
    {
        $this->codigoViaje = $codigoViaje;

        return $this;
    }

    public function getNumeroPlazas(): ?int
    {
        return $this->numeroPlazas;
    }

    public function setNumeroPlazas(int $numeroPlazas): self
    {
        $this->numeroPlazas = $numeroPlazas;

        return $this;
    }

    public function getOrigen(): ?string
    {
        return $this->origen;
    }

    public function setOrigen(string $origen): self
    {
        $this->origen = $origen;

        return $this;
    }

    public function getDestino(): ?string
    {
        return $this->destino;
    }

    public function setDestino(string $destino): self
    {
        $this->destino = $destino;

        return $this;
    }

    public function getPrecio(): ?float
    {
        return $this->precio;
    }

    public function setPrecio(float $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    public function getBorrado(): ?int
    {
        return $this->borrado;
    }

    public function setBorrado(int $borrado): self
    {
        $this->borrado = $borrado;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }


}
