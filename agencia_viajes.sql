-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.11-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para agencia_viajes
CREATE DATABASE IF NOT EXISTS `agencia_viajes` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `agencia_viajes`;

-- Volcando estructura para tabla agencia_viajes.viajeros
CREATE TABLE IF NOT EXISTS `viajeros` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `cedula` varchar(50) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `borrado` int(10) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla agencia_viajes.viajeros: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `viajeros` DISABLE KEYS */;
/*!40000 ALTER TABLE `viajeros` ENABLE KEYS */;

-- Volcando estructura para tabla agencia_viajes.viajero_viajes
CREATE TABLE IF NOT EXISTS `viajero_viajes` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `viajero_id` int(20) NOT NULL,
  `viaje_id` int(20) NOT NULL,
  `borrado` int(20) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `viaje_fk` (`viaje_id`),
  KEY `viajero_fk` (`viajero_id`),
  CONSTRAINT `viaje_fk` FOREIGN KEY (`viaje_id`) REFERENCES `viajes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `viajero_fk` FOREIGN KEY (`viajero_id`) REFERENCES `viajeros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla agencia_viajes.viajero_viajes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `viajero_viajes` DISABLE KEYS */;
/*!40000 ALTER TABLE `viajero_viajes` ENABLE KEYS */;

-- Volcando estructura para tabla agencia_viajes.viajes
CREATE TABLE IF NOT EXISTS `viajes` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `codigo_viaje` varchar(50) NOT NULL,
  `numero_plazas` int(20) NOT NULL,
  `origen` varchar(50) NOT NULL,
  `destino` varchar(50) NOT NULL,
  `precio` float(10,2) NOT NULL,
  `borrado` int(10) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla agencia_viajes.viajes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `viajes` DISABLE KEYS */;
/*!40000 ALTER TABLE `viajes` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
